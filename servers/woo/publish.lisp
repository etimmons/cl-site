(in-package :cl-site-woo)

(defparameter *listeners* 5) 

(defun print-unimplemented-message ()
  (format t "

**********************************

Thank you for trying the clnet site Woo test server!

Woo itself is loaded and ready to go! But, the specific code
for serving the cl-site pages for Woo has not been filled in
yet.

If you'd like to volunteer to do this, and maintain it, please fill in
the `publish-cl-site,' `client-test,' and `start-server' functions in
the file cl-site/servers/woo/publish.lisp.

Any changes/additions for Woo support should be contained in
this directory (cl-site/servers/woo/), except for the definition and
exporting of a global variable specifying a distinct Woo
default starting port `cl-site-woo:*port*,' which can go in
cl-site/globals.lisp and cl-site/package.lisp respectively.


For implementing the functions below, it may help to follow the
examples in cl-site/servers/paserve/publish.lisp.

**********************************

"))


(defun publish-cl-site (&key (output-dir cl-site:*OUTPUT-DIR*))
  "This function should publish all the site content from the output-dir, both with and without their .html suffices"
  (declare (ignore ouput-dir)))


(defun client-test (port)
  "This function should check whether the given port is free for listening, and return it if so, nil if not."
  (declare (ignore port)))


(defun start-server (&key (port ;;cl-site:*woo-port* ;; FLAG Define this global and uncomment this when activated.
			   ) (listeners *listeners*))
  "This function should check for the next available port, starting with the given port, and start a webserver
on that port. If starting on that port throws an error, then subsequent ports should be tried, with randomly 
increasing wait times in between (a la Ethernet collision avoidance strategy)."
  (declare (ignore port listeners))
  (print-unimplemented-message))
  





