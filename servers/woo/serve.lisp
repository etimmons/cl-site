(in-package :cl-user)

(uiop:with-current-directory (*load-truename*)
  (load "../../build"))

(ql:quickload :cl-site-woo)

;;
;; Start aserve, publish site directory & pages, set default package.
;;
(cl-site-woo:start-server)
(cl-site-woo:publish-cl-site)
(setq *package* (find-package :cl-site))

(format t "

This message from cl-site/servers/woo/serve.lisp should report the url & port 
that the user may visit to browse the site.

")

